(*
	BSD 2-Clause License
	
	Copyright (c) 2022, Vasile Guta-Ciucur
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.
	
	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
*)

unit myrayguicontrols;
{$mode objfpc}{$H+}

interface

uses sysutils,raylib,raygui;

var
  fontLCD, fontLCD2, fontTitle, fontSButton : TFont;
  fontLbl, fontLbl2, fontButton, fontButtonBold : TFont;
  fontLetter, fontLabel, fontDesc: TFont;
  mousePosition : TVector2;

function GuiGradientButton(bounds: TRectangle; const text: PChar; ButtonActive: Boolean): Boolean;
function GuiGradientButtonB(bounds: TRectangle; const text: PChar; ButtonActive: Boolean): Boolean;
//
function GuiSpeedButton(sbCoord: TVector2; sbWidth: Single; activePix, disabledPix: TTexture2D; ButtonActive: boolean): Boolean;
function GuiSpeedButtonLbl(sbCoord: TVector2; sbWidth: Single; text :PChar; activePix, disabledPix: TTexture2D; ButtonActive: boolean): Boolean;

procedure designer;

implementation

function GuiGradientButton(bounds: TRectangle; const text: PChar; ButtonActive: Boolean): Boolean;
var
  state : TGuiControlState;
  pressed, IsGuiLocked : Boolean;
  colorstart, colorend: TColor;
  mousepos, bcoord, myStringSize: TVector2;
  x,y,w,h:Integer;

begin
  pressed := false;
  if (ButtonActive) then begin
    state := GuiGetState;
    IsGuiLocked := GuiIsLocked;
  end
  else begin
    state := STATE_DISABLED;
    IsGuiLocked := true;
  end;

  if ((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    mousepos := GetMousePosition;
    if(CheckCollisionPointRec(mousepos, bounds)) then begin
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)) then state := STATE_PRESSED
      else state := STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) then pressed := true;
    end;
  end;

  if (state = STATE_NORMAL) then begin
    //
    //
    colorstart.r:= $85;
    colorstart.g:= $85;
    colorstart.b:= $85;
    colorstart.a:= 48;
    //
    colorend.r:= $38;
    colorend.g:= $38;
    colorend.b:= $38;
    colorend.a:= 48;
  end;
  if (state = STATE_FOCUSED) then begin
    //colorstart := elementStyle; // f5ff97
    //colorend := elementStyle;   // 71792b
    colorstart.r:= $f5;
    colorstart.g:= $ff;
    colorstart.b:= $97;
    colorstart.a:= 96;
    //
    colorend.r:= $71;
    colorend.g:= $79;
    colorend.b:= $2b;
    colorend.a:= 96;
  end;
  if (state = STATE_PRESSED) then begin
    //colorstart := elementStyle; // ffd490
    //colorend := elementStyle;   // b38439
    colorend.r:= $ff;
    colorend.g:= $d4;
    colorend.b:= $90;
    colorend.a:= 96;
    //
    colorstart.r:= $b3;
    colorstart.g:= $84;
    colorstart.b:= $39;
    colorstart.a:= 96;
  end;
  if (state = STATE_DISABLED) then begin
    //
    //
    colorstart.r:= $85;
    colorstart.g:= $85;
    colorstart.b:= $85;
    colorstart.a:= 48;
    //
    colorend.r:= $38;
    colorend.g:= $38;
    colorend.b:= $38;
    colorend.a:= 48;
  end;

  x := StrToInt(FloatToStr(bounds.x));
  y := StrToInt(FloatToStr(bounds.y));
  w := StrToInt(FloatToStr(bounds.width));
  h := StrToInt(FloatToStr(bounds.height));
  DrawRectangleGradientV(x, y, w, h, colorstart, colorend);

  if ((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    myStringSize := MeasureTextEx(fontButton, text, 16, 2);
    bcoord.x := (bounds.width / 2) - (myStringSize.x / 2) + bounds.x;
    bcoord.y := (bounds.height / 2) - (myStringSize.y / 2) + bounds.y;

    DrawTextEx(fontButton, text, bcoord, 16, 2, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_NORMAL)));
  end;
  GuiGradientButton := pressed;
end;

function GuiGradientButtonB(bounds: TRectangle; const text: PChar; ButtonActive: Boolean): Boolean;
var
  state : TGuiControlState;
  pressed, IsGuiLocked : Boolean;
  colorstart, colorend: TColor;
  mousepos, bcoord, myStringSize, myStringSizeBold: TVector2;
  x, y, w, h: Integer;

begin
  myStringSize := MeasureTextEx(fontButton, text, 16, 2);
  myStringSizeBold := MeasureTextEx(fontButtonBold, text,16, 2);

  pressed := false;
  if (ButtonActive) then begin
    state := GuiGetState;
    IsGuiLocked := GuiIsLocked;
  end
  else begin
    state := STATE_DISABLED;
    IsGuiLocked := true;
  end;

  if ((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    mousepos := GetMousePosition;
    if(CheckCollisionPointRec(mousepos, bounds)) then begin
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)) then begin
        state := STATE_PRESSED;
      end else state := STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) then pressed := true;
    end;
  end;

  if (state = STATE_NORMAL) then begin
    //
    //
    colorstart.r:= $85;
    colorstart.g:= $85;
    colorstart.b:= $85;
    colorstart.a:= 48;
    //
    colorend.r:= $38;
    colorend.g:= $38;
    colorend.b:= $38;
    colorend.a:= 48;
  end;
  if (state = STATE_FOCUSED) then begin
    //colorstart := elementStyle; // f5ff97
    //colorend := elementStyle;   // 71792b
    colorstart.r:= $f5;
    colorstart.g:= $ff;
    colorstart.b:= $97;
    colorstart.a:= 96;
    //
    colorend.r:= $71;
    colorend.g:= $79;
    colorend.b:= $2b;
    colorend.a:= 96;
  end;
  if (state = STATE_PRESSED) then begin
    //colorstart := elementStyle; // ffd490
    //colorend := elementStyle;   // b38439
    colorend.r:= $ff;
    colorend.g:= $d4;
    colorend.b:= $90;
    colorend.a:= 96;
    //
    colorstart.r:= $b3;
    colorstart.g:= $84;
    colorstart.b:= $39;
    colorstart.a:= 96;
  end;
  if (state = STATE_DISABLED) then begin
    //
    //
    colorstart.r:= $85;
    colorstart.g:= $85;
    colorstart.b:= $85;
    colorstart.a:= 48;
    //
    colorend.r:= $38;
    colorend.g:= $38;
    colorend.b:= $38;
    colorend.a:= 48;
  end;

  x := StrToInt(FloatToStr(bounds.x));
  y := StrToInt(FloatToStr(bounds.y));
  w := StrToInt(FloatToStr(bounds.width));
  h := StrToInt(FloatToStr(bounds.height));
  DrawRectangleGradientV(x, y, w, h, colorstart, colorend);

  if (state <> STATE_DISABLED) then begin
    if (state = STATE_PRESSED) then begin
      bcoord.x := ((bounds.width / 2) - (myStringSizeBold.x / 2) + bounds.x) + 2;
      bcoord.y := ((bounds.height / 2) - (myStringSizeBold.y / 2) + bounds.y) + 2;
      DrawTextEx(fontButtonBold, text, bcoord, 16, 2, BLACK);
      bcoord.x := bcoord.x - 2;
      bcoord.y := bcoord.y - 2;
      DrawTextEx(fontButtonBold, text, bcoord, 16, 2, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_FOCUSED)));
    end else begin
      bcoord.x := ((bounds.width / 2) - (myStringSize.x / 2) + bounds.x);
      bcoord.y := ((bounds.height / 2) - (myStringSize.y / 2) + bounds.y);
      DrawTextEx(fontButton, text, bcoord, 16, 2, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_NORMAL)));
    end;
  end else begin
    bcoord.x := ((bounds.width / 2) - (myStringSize.x / 2) + bounds.x);
    bcoord.y := ((bounds.height / 2) - (myStringSize.y / 2) + bounds.y);
    DrawTextEx(fontButton, text, bcoord, 16, 2, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_DISABLED)));
  end;
  GuiGradientButtonB := pressed;
end;

// =============================================================================

function GuiSpeedButton(sbCoord: TVector2; sbWidth: single; activePix, disabledPix: TTexture2D; ButtonActive: boolean): Boolean;
var
  state : TGuiControlState;
  pressed, IsGuiLocked : Boolean;
  bounds : TRectangle;
  mousepos: TVector2;
  x,y:Integer;
begin
  bounds.x := sbCoord.x;
  bounds.y := sbCoord.y;
  bounds.width := sbWidth;
  bounds.height := sbWidth;

  pressed := false;
  if (ButtonActive) then begin
    state := GuiGetState;
    IsGuiLocked := GuiIsLocked;
  end
  else begin
    state := STATE_DISABLED;
    IsGuiLocked := true;
  end;

  if ((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    mousepos := GetMousePosition;
    if(CheckCollisionPointRec(mousepos, bounds)) then begin
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)) then
        state := STATE_PRESSED
      else
        state := STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) then pressed := true;
    end;
  end;

  x := StrToInt(FloatToStr(bounds.x));
  y := StrToInt(FloatToStr(bounds.y));

  if((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    case state of
      STATE_NORMAL: begin
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_NORMAL)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_NORMAL)));
      end;
      STATE_FOCUSED: begin
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
      end;
      STATE_PRESSED: begin
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
      end;
    end;
    DrawTexture(activePix, x+2, y+2, WHITE);
  end
  else begin
    //DrawRectangle(x+2,y+2,32,32,GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_DISABLED)));
    DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_DISABLED)));
    DrawTexture(disabledPix, x+2, y+2, WHITE);
  end;


  GuiSpeedButton := pressed;
end;

function GuiSpeedButtonLbl(sbCoord: TVector2; sbWidth: Single; text :PChar; activePix, disabledPix: TTexture2D; ButtonActive: boolean): Boolean;
var
  state : TGuiControlState;
  pressed, IsGuiLocked : Boolean;
  bounds : TRectangle;
  mousepos, bcoord: TVector2;
  x,y:Integer;
begin
  bounds.x := sbCoord.x;
  bounds.y := sbCoord.y;
  bounds.width := sbWidth;
  bounds.height := sbWidth;

  pressed := false;
  if (ButtonActive) then begin
    state := GuiGetState;
    IsGuiLocked := GuiIsLocked;
  end
  else begin
    state := STATE_DISABLED;
    IsGuiLocked := true;
  end;

  if ((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    mousepos := GetMousePosition;
    if(CheckCollisionPointRec(mousepos, bounds)) then begin
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)) then
        state := STATE_PRESSED
      else
        state := STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) then pressed := true;
    end;
  end;

  x := StrToInt(FloatToStr(bounds.x));
  y := StrToInt(FloatToStr(bounds.y));

  if((state <> STATE_DISABLED) and (not IsGuiLocked)) then begin
    case state of
      STATE_NORMAL: begin
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_NORMAL)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_NORMAL)));
      end;
      STATE_FOCUSED: begin
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
      end;
      STATE_PRESSED: begin
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
      end;
    end;
    DrawTexture(activePix, x+2, y+2, WHITE);
  end
  else begin
    //DrawRectangle(x+2,y+2,32,32,GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_DISABLED)));
    DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_DISABLED)));
    DrawTexture(disabledPix, x+2, y+2, WHITE);
  end;
  bcoord.x := sbCoord.x + 2;
  bcoord.y := sbCoord.y + 39;
  DrawTextEx(fontSButton, text, bcoord, 14, 1, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_NORMAL)));

  GuiSpeedButtonLbl := pressed;
end;

procedure designer;
begin
  //==================== GUI ASSISTANT =========================
  // live ui design :) - assistive elements!!! - really life saver - otherwise, you have to use godot to design the UI
  // - used to find out the coordinates for speedbuttons,
  //   moving the mouse at the desired location and reading
  //   the coordinates (dragging along a 32x32 rectangle that
  //    simulated the speedbutton then used also for other elements)
  //
  DrawText('x = ', 0,0,16,WHITE);
  DrawText('y = ', 0,20,16,WHITE);
  DrawText(PChar(IntToStr(round(mousePosition.x))), 56,0,16,WHITE);
  DrawText(PChar(IntToStr(round(mousePosition.y))), 56,20,16,WHITE);
  DrawLine(0,round(mousePosition.y),1365,round(mousePosition.y), WHITE);
  DrawLine(round(mousePosition.x),0,round(mousePosition.x),705,WHITE);
  // resize the rectangle below as per dimensions of your widget:
  DrawRectangle(round(mousePosition.x), round(mousePosition.y),32,32,ORANGE);
  // or uncomment the following if you need to place a label:
  //DrawTextEx(fontSButton, '123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234567890', mousePosition, 14, 1, clLetter);
  // and write down the coordonates from the upper left corner of the screen.
  // I move (or just copy) this around, from a screen to another
  //   to help me in layouting the "forms".
  //================= end live ui design =======================
end;

end.
