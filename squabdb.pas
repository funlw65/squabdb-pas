
Program squabdb;

{
  A list of Performing Pigeons from Europe.
  * This is a demonstration for typhoon database and for
  * raylib 4.2 + raygui 3.2 graphic libraries.
  * Copyright 2022 by Vasile Guţă-Ciucur and Lorena Guţă-Ciucur
  * Released under BSD license
}
{$mode objfpc}{$H+}

Uses 
cmem,
sysutils,
  {$define TYPHOON_STATIC}
ty,
datatydb,
  {$define RAY_STATIC}
raylib,
raygui,
myrayguicontrols
;

Const 
  screenWidth = 1366;
  // Laptop screen
  screenHeight = 706;
  // Panels' position on the screen
  pSplash_posX = 332;
  pSplash_posY = 136;
  pMain_posX = 213;
  pMain_posY = 40;
  // Define the Application's colors
  //clTeal : TColor = (r: 97; g: 190; b: 221; a: 255);
  //clGray : TColor = (r: 206; g: 206; b: 206; a: 255);
  //clBlueGray : TColor  = (r: 231; g: 231; b: 239; a: 255);
  //clGreen : TColor = (r: 180; g: 192; b: 69; a: 255);
  //clLCD: TColor = (r: 50; g: 50; b: 50; a: 255);
  clDarkBlue : TColor = (r: 45; g: 43; b: 56; a: 255);
  //cllightBlue : TColor = (r: 51; g: 102; b: 152; a: 255); //#336698
  clAlmostBlack : TColor = (r: 33; g: 33; b: 33; a: 255);
  //clCreme : TColor = (r: 223; g: 196; b: 125; a: 255);
  //clPhotoFrame :TColor = (r: 45; g: 57; b: 95; a: 255);
  clBrick : TColor = (r: 206; g: 72; b: 0; a: 255);
  //clGrass : TColor = (r: 98; g: 187; b: 45; a: 255);
  //clRed


Var 
  currentScreen, originated : integer;

  // the main colors of the application
  clBackground,
  clPanel,
  clLabels,
  clLetter,
  clFrames,
  clPhoto : TColor;


  bufferIcon : TImage;
  bufferTex, bufferTex2, iNoImage,
  //speedbutton images
  sbFirst,
  sbPrev,
  sbNext,
  sbLast,
  sbFind,
  sbAdd,
  sbMod,
  sbDel,
  sbInfo,
  sbExit,
  //
  sbFirst_gray,
  sbPrev_gray,
  sbNext_gray,
  sbLast_gray,
  sbFind_gray,
  sbAdd_gray,
  sbMod_gray,
  sbDel_gray,
  //
  iPigeon_Welcome,
  iPigeon_Bye,
  iGrass2b,
  iGrass1a,
  iSlime1,
  iSlime2,
  iAbout,

  iFirefly1,
  iFirefly2,
  iLadybug,
  iFindLarge,
  iAddLarge,
  iModLarge
  : TTexture2D;


  // status buttons
  bSplashClose_click,
  bQuitYes_click,
  bQuitNo_click,
  bErrorNextKey_click,
  bErrorFindKey_click,
  bDelYes_click,
  bDelNo_click,
  bFindKey_click,
  bResultDel_click,
  bResultMod_click,
  bResultClose_click,
  bResultDelYes_click,
  bResultDelNo_click,
  //
  bAddSave_click,
  bAddCancel_click,
  bErrorAddKey_click,
  //
  bModSave_click,
  bModCancel_click,
  //
  //speedbutton enabled/disabled
  sbFirst_active,
  sbPrev_active,
  sbNext_active,
  sbLast_active,
  sbFind_active,
  sbAdd_active,
  sbMod_active,
  sbDel_active,
  //
  sbFirst_click,
  sbPrev_click,
  sbNext_click,
  sbLast_click,
  sbFind_click,
  sbAdd_click,
  sbMod_click,
  sbDel_click,
  sbInfo_click,
  sbExit_click
  : boolean;

  iSearchEdit: boolean = false;
  iBreedEdit: boolean = false;
  iCategoryEdit: boolean = false;
  iOriginEdit: boolean = false;
  iImageidEdit: boolean = false;
  iDesc1Edit: boolean = false;
  iDesc2Edit: boolean = false;
  //
  exitWindow : boolean;
  // detecting the speedbutton collision with mouse pointer at mouseclick
  rec_sbFirst,
  rec_sbPrev,
  rec_sbNext,
  rec_sbLast,
  rec_sbFind,
  rec_sbAdd,
  rec_sbMod,
  rec_sbDel,
  rec_sbInfo,
  rec_sbExit : TVector2;
  // combobox
  //rec_cbTheme,
  rec_photoFrame,
  rec_common
  : TRectangle;

  // combobox_index
  cbTheme_idx : longint;
  //cbTheme_edit : boolean;

  commonPos, commonPos2 : TVector2;
  fontNil : PLongInt;

  stp : integer;

  // pigeon database record structure
  project_subfolder,
  breed,breed_buff,
  cat,cat_buff,
  orig,orig_buff,
  desc1,desc1_buff,
  desc2,desc2_buff,
  imgid,imgid_buff: string;
  //
  brd : breeds;
  cr_rec : DB_ADDR;
  //
  squabdb_empty, cycle_common : boolean;
  //
  i, ln, add_err, mod_err : byte;

  err_msg,
  image_path: string;
  //

  //
  eText : array [0..59] Of char = '';
  //
  eBreed    : array [0..59] Of char = '';
  eCategory : array [0..10] Of char = '';
  eOrigin   : array [0..59] Of char = '';
  eImageid  : array [0..59] Of char = '';
  eDesc1    : array [0..99] Of char = '';
  eDesc2    : array [0..99] Of char = '';
  eDescription    : array [0..159] Of char = '';
  //
  eBreed_buff    : array [0..59] Of char = '';
  eCategory_buff : array [0..10] Of char = '';
  eOrigin_buff   : array [0..59] Of char = '';
  eImageid_buff  : array [0..59] Of char = '';
  eDesc1_buff    : array [0..99] Of char = '';
  eDesc2_buff    : array [0..99] Of char = '';
  eDescription_buff    : array [0..159] Of char = '';
  //

Procedure clear_mod_fields;
Begin
  eBreed_buff    := '';
  eCategory_buff := '';
  eOrigin_buff   := '';
  eImageid_buff  := '';
  eImageid_buff  := '';
  eDesc1_buff    := '';
  eDesc2_buff    := '';
End;

Procedure clear_add_fields;
Begin
  eBreed    := '';
  eCategory := '';
  eOrigin   := '';
  eImageid  := '';
  eDesc1    := '';
  eDesc2    := '';
End;

Procedure clear_buffer_fields;
Begin
  breed_buff := '';
  cat_buff   := '';
  orig_buff  := '';
  desc1_buff  := '';
  desc2_buff  := '';
  imgid_buff := '';
End;

Procedure clear_fields;
Begin
  breed := '';
  cat   := '';
  orig  := '';
  desc1  := '';
  desc2  := '';
  imgid := '';
End;


Procedure load_imageid(Var im: String; Var bTex:TTexture2D);
Begin
  UnloadTexture(bTex);
  If im <> '' Then
    Begin
      image_path := trim(project_subfolder + im);
      If (FileExists(@image_path[1])) Then
        Begin
          bufferIcon := LoadImage(@image_path[1]);
          bTex := LoadTextureFromImage(bufferIcon);
          UnloadImage(bufferIcon);
        End
      Else
        Begin
          bufferIcon := LoadImage('pictures/noimage.png');
          bTex := LoadTextureFromImage(bufferIcon);
          UnloadImage(bufferIcon);
        End;
    End
  Else
    Begin
      bufferIcon := LoadImage('pictures/noimage.png');
      bTex := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
    End;
End;

Procedure prepare_fields;
Begin
  d_crget(@cr_rec);
  eCategory := cat;
  eOrigin := orig;
  eImageid := imgid;
  eDesc1 := desc1;
  eDesc2 := desc2;
End;

Begin
  //--------------------------------------------------------------------
  // Database stuff
  //--------------------------------------------------------------------
  clear_fields;
  clear_buffer_fields;
  clear_mod_fields;
  clear_add_fields;
  project_subfolder := 'pictures/';
  //
  {$I-}
  If (Not DirectoryExists('data')) Then
    MkDir('data');
  {$I+}
  //
  d_dbfpath('data');
  If ( d_open('datatydb', 'x') = S_OKAY ) Then
    Begin
      If (d_keyfrst(NAME_fld) = S_OKAY) Then
        Begin
          d_recread(@brd);
          breed := brd.name;
          cat := brd.category;
          orig := brd.origin;
          imgid := brd.imageid;
          desc1 := brd.desc1;
          desc2 := brd.desc2;
          squabdb_empty  := false;
          sbFirst_active := false;
          sbPrev_active  := false;
          sbNext_active  := true;
          sbLast_active  := true;
          sbFind_active  := true;
          sbAdd_active   := true;
          sbMod_active   := true;
          sbDel_active   := true;
        End
      Else
        Begin
          squabdb_empty := true;
          clear_fields;
        End;
      // -------------------------------------------------------------------
      // Raylib Initialization
      // -------------------------------------------------------------------
      InitWindow(screenWidth, screenHeight, 'SquabDB - Pigeon Database');
      SetTargetFPS(16);
      // Set our "game" to run at 17 frames-per-second
      GuiSetStyle(DEFAULT, TEXT_SIZE, 16);
      GuiSetState(STATE_NORMAL);
      exitWindow := false;
      SetExitKey(0);

      fontNil := Nil;
      fontTitle   := LoadFontEx('style/Accanthis.otf', 56, fontNil, 0);
      fontLetter  := LoadFontEx('style/Accanthis.otf', 24, fontNil, 0);
      fontLabel   := LoadFontEx('style/FreeSans.ttf', 20, fontNil, 0);
      fontButton  := LoadFontEx('style/FreeSans.ttf', 16, fontNil, 0);
      fontButtonBold := LoadFontEx('style/FreeSansBold.ttf', 16, fontNil, 0);
      fontSButton := LoadFontEx('style/Mecha.ttf', 14, fontNil, 0);
      fontDesc    := LoadFontEx('style/Mecha.ttf', 16, fontNil, 0);

      bufferIcon := LoadImage('pictures/noimage.png');
      iNoImage   := LoadTextureFromImage(bufferIcon);
      bufferTex  := LoadTextureFromImage(bufferIcon);
      bufferTex2 := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      load_imageid(imgid, bufferTex);

      // Splash
      bufferIcon := LoadImage('img/Pigeon.png');
      iPigeon_Welcome := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      // Exit
      bufferIcon := LoadImage('img/Flying_Pigeon.png');
      iPigeon_Bye := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/slime1.png');
      iSlime1 := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/slime2.png');
      iSlime2 := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      // About
      bufferIcon := LoadImage('img/about.png');
      iAbout := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/grass1a.png');
      iGrass1a := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/grass2b.png');
      iGrass2b := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/firefly1.png');
      iFirefly1 := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/firefly2.png');
      iFirefly2 := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/ladybug.png');
      iLadybug := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/128/iLfind.png');
      iFindLarge := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/general/128/iLmod.png');
      // downgraded to 64x64, aestetic reasons
      iModLarge := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      bufferIcon := LoadImage('img/general/iLadd.png');
      // downgraded to 64x64
      iAddLarge := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);

      // load the icons of emulated speedbuttons
      bufferIcon := LoadImage('img/general/first.png');
      sbFirst := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/prev.png');
      sbPrev := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/next.png');
      sbNext := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/last.png');
      sbLast := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/find.png');
      sbFind := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/add.png');
      sbAdd := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/mod.png');
      sbMod := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/del.png');
      sbDel := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/info.png');
      sbInfo := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/quit.png');
      sbExit := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      // _gray
      bufferIcon := LoadImage('img/general/first_gray.png');
      sbFirst_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/prev_gray.png');
      sbPrev_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/next_gray.png');
      sbNext_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/last_gray.png');
      sbLast_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/find_gray.png');
      sbFind_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/add_gray.png');
      sbAdd_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/mod_gray.png');
      sbMod_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);
      //
      bufferIcon := LoadImage('img/general/del_gray.png');
      sbDel_gray := LoadTextureFromImage(bufferIcon);
      UnloadImage(bufferIcon);


      // simulating clicks
      bSplashClose_click := false;
      bQuitYes_click := false;
      bQuitNo_click := false;
      bErrorNextKey_click := false;


      // rectangle areas of the simulated speedbuttons...

      rec_sbFirst.x := 252 - 48;
      rec_sbFirst.y := 72;

      rec_sbPrev.x := 252;
      rec_sbPrev.y := 72;

      rec_sbNext.x := 300;
      rec_sbNext.y := 72;

      rec_sbLast.x := 348;
      rec_sbLast.y := 72;

      rec_sbFind.x := 348 + 48;
      rec_sbFind.y := 72;

      rec_sbAdd.x := 396 + 48;
      rec_sbAdd.y := 72;

      rec_sbMod.x := 444 + 48;
      rec_sbMod.y := 72;

      rec_sbDel.x := 492 + 48;
      rec_sbDel.y := 72;

      rec_sbInfo.x := 540 + 48;
      rec_sbInfo.y := 72;

      rec_sbExit.x := 588 + 48;
      rec_sbExit.y := 72;

      // combobox rectangle
      //rec_cbTheme.x := pSplash_posX + 150;
      //rec_cbTheme.y := pSplash_posY + 398;
      //rec_cbTheme.width := 130;
      //rec_cbTheme.height := 32;
      //
      rec_photoFrame.x := 677 + 48;
      rec_photoFrame.y := 72;
      rec_photoFrame.width := 400;
      rec_photoFrame.height := 400;


      // combobox_index
      cbTheme_idx := 0;
      //cbTheme_edit := false;

      currentScreen := -1;

      GuiLoadStyle('style/squabdb_dark.rgs');
      clBackground := clDarkBlue;
      clPanel := clAlmostBlack;
      clLabels := LIGHTGRAY;
      clLetter := WHITE;
      clFrames := LIGHTGRAY;
      clPhoto := DARKGRAY;
      //--------------------------------------------------------------------
      // Main game loop
      While Not exitWindow Do
        Begin
          // Update
          //----------------------------------------------------------------
          mousePosition.x := GetMouseX;
          mousePosition.y := GetMouseY;

          //----------------------------------------------------------------
          // Draw
          //----------------------------------------------------------------
          BeginDrawing();
          // =====================================================================
          // SPLASH SCREEN
          // =====================================================================
          If currentScreen = -1 Then
            Begin
              // draw splah
              ClearBackground(clBackground);
              DrawRectangle(pSplash_posX,pSplash_posY,696,664-204,clPanel);
              DrawTexture(iPigeon_Welcome, 200,100, WHITE);
              commonPos.x := single(pSplash_posX + 280);
              commonPos.y := single(pSplash_posY + 50);
              DrawTextEx(fontTitle, 'Welcome!', commonPos, 56, 2, clLabels);
              //
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 150);
              DrawTextEx(fontLetter, 'SquabDB is a little Pigeon database, intended as', commonPos,
                         24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 174);
              DrawTextEx(fontLetter, 'demonstrator for typhoon database engine, raylib', commonPos,
                         24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 198);
              DrawTextEx(fontLetter, 'and raygui graphical libraries. ', commonPos, 24, 2, clLabels)
              ;
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 222);
              DrawTextEx(fontLetter, 'Pigeon cartoons are licensed to: ', commonPos, 24, 2, clLabels
              );
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 246);
              DrawTextEx(fontLetter, '   - David StClair (Wildchief)', commonPos, 24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 270);
              DrawTextEx(fontLetter, '   - Dmitry Moiseenko', commonPos, 24, 2, clLabels);
              //
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 314);
              DrawTextEx(fontLetter, 'Copyright 2022 by Vasile Guta-Ciucur.', commonPos, 24, 2,
                         clLabels);
              commonPos.x := single(pSplash_posX + 180);
              commonPos.y := single(pSplash_posY + 338);
              DrawTextEx(fontLetter, 'Application is BSD licensed.', commonPos, 24, 2, clLabels);
              //
              //commonPos.x := single(pSplash_posX + 80);
              //commonPos.y := single(pSplash_posY + 406);
              //DrawTextEx(fontLabel, 'Theme:', commonPos, 20, 1, clLabels);
              //
              bSplashClose_click := GuiGradientButtonB(RectangleCreate(pSplash_posX + 530,
                                    pSplash_posY + 398, 88, 32), 'Close', true);
              If bSplashClose_click Then
                Begin
                  bSplashClose_click := false;
                  currentScreen := 0;
                End;
            End;
          //end page/screen
          // =====================================================================
          // MAIN PANEL !!!
          // =====================================================================
          If currentScreen = 0 Then
            Begin
              originated := 0;
              If squabdb_empty Then
                Begin
                  //if database is empty
                  sbFirst_active := false;
                  sbPrev_active := false;
                  sbNext_active := false;
                  sbLast_active := false;
                  sbFind_active := false;
                  sbAdd_active := true;
                  sbMod_active := false;
                  sbDel_active := false;
                End;
              //exitWindow := WindowShouldClose;
              ClearBackground(clBackground);
              DrawTexture(iGrass1a, pMain_posX-82-48, pMain_posY + 425, WHITE);
              DrawRectangle(pMain_posX-48,pMain_posY,896+96,600,clPanel);
              // MAIN PANEL
              DrawTexture(iGrass2b, pMain_posX+802+48, pMain_posY + 423, WHITE);
              //
              sbFirst_click := GuiSpeedButtonLbl(rec_sbFirst, 36.0, 'First', sbFirst, sbFirst_gray,
                               sbFirst_active);
              sbPrev_click  := GuiSpeedButtonLbl(rec_sbPrev, 36.0, 'Prev', sbPrev, sbPrev_gray,
                               sbPrev_active);
              sbNext_click  := GuiSpeedButtonLbl(rec_sbNext, 36.0, 'Next', sbNext, sbNext_gray,
                               sbNext_active);
              sbLast_click  := GuiSpeedButtonLbl(rec_sbLast, 36.0, 'Last', sbLast, sbLast_gray,
                               sbLast_active);
              sbFind_click  := GuiSpeedButtonLbl(rec_sbFind, 36.0, 'Find', sbFind, sbFind_gray,
                               sbFind_active);
              sbAdd_click   := GuiSpeedButtonLbl(rec_sbAdd, 36.0, 'Add', sbAdd, sbAdd_gray,
                               sbAdd_active);
              sbMod_click   := GuiSpeedButtonLbl(rec_sbMod, 36.0, 'Mod', sbMod, sbMod_gray,
                               sbMod_active);
              sbDel_click   := GuiSpeedButtonLbl(rec_sbDel, 36.0, 'Del', sbDel, sbDel_gray,
                               sbDel_active);
              sbInfo_click  := GuiSpeedButtonLbl(rec_sbInfo, 36.0, 'Info', sbInfo, sbInfo, true);
              // never gets disabled so no gray pixmap
              sbExit_click  := GuiSpeedButtonLbl(rec_sbExit, 36.0, 'Exit', sbExit, sbExit, true);
              // -"-
              //
              // labels and fields
              commonPos.x := 252-48;
              commonPos.y := 181;
              DrawTextEx(fontLabel, 'BREED NAME:', commonPos, 20, 1, clLabels);
              commonPos.x := 270-48;
              commonPos.y := 215;
              If breed = '' Then
                DrawTextEx(fontLetter, '...', commonPos, 24, 1, clLabels)
              Else
                DrawTextEx(fontLetter, @breed[1], commonPos, 24, 1, clLabels);
              commonPos.x := 252-48;
              commonPos.y := 261;
              DrawTextEx(fontLabel, 'CATEGORY:', commonPos, 20, 1, clLabels);
              commonPos.x := 270-48;
              commonPos.y := 295;
              If cat = '' Then
                DrawTextEx(fontLetter, '...', commonPos, 24, 1, clLabels)
              Else
                DrawTextEx(fontLetter, @cat[1], commonPos, 24, 1, clLabels);
              commonPos.x := 252-48;
              commonPos.y := 341;
              DrawTextEx(fontLabel, 'ORIGIN:', commonPos, 20, 1, clLabels);
              commonPos.x := 270-48;
              commonPos.y := 375;
              If orig = '' Then
                DrawTextEx(fontLetter, '...', commonPos, 24, 1, clLabels)
              Else
                DrawTextEx(fontLetter, @orig[1], commonPos, 24, 1, clLabels);
              commonPos.x := 252-48;
              commonPos.y := 506;
              DrawTextEx(fontLabel, 'DESCRIPTION:', commonPos, 20, 1, clLabels);
              commonPos.x := 270-48;
              commonPos.y := 544;
              DrawTextEx(fontDesc, @desc1[1], commonPos, 16, 1, clLabels);
              commonPos.x := 270-48;
              commonPos.y := 584;
              DrawTextEx(fontDesc, @desc2[1], commonPos, 16, 1, clLabels);
              //
              // draw lines and photo frame
              commonPos.x := single(pMain_posX-48);
              commonPos.y := single(pMain_posY+110);
              commonPos2.x := commonPos.x + 896.0+96;
              commonPos2.y := commonPos.Y;
              DrawLineEx(commonPos, commonPos2, 4.0, clFrames);
              DrawRectangleRounded(rec_photoFrame,0.1,4,clPhoto);
              commonPos.x := single(rec_photoFrame.x + 176);
              commonPos.y := single(rec_photoFrame.y + 410);
              DrawTextEx(fontSButton, '372 x 372', commonPos, 14, 2, clLetter);
              If imgid = '' Then
                DrawTexture(iNoImage,691+48,86,WHITE)
              Else
                DrawTexture(bufferTex,691+48,86,WHITE);

              // MOUSE EVENT!!!
              If (sbFirst_click) Then
                currentScreen := 1;
              If (sbPrev_click) Then
                currentScreen := 2;
              If (sbNext_click) Then
                currentScreen := 3;
              If (sbLast_click) Then
                currentScreen := 4;
              If (sbFind_click) Then
                currentScreen := 5;
              If (sbAdd_click) Then
                Begin
                  currentScreen := 6;
                  // first, goto (a little detour) last record
                  clear_add_fields;
                  clear_mod_fields;
                End;
              If (sbMod_click)  Then
                Begin
                  currentScreen := 7;
                  prepare_fields;
                End;
              If (sbDel_click) Then
                currentScreen := 8;
              If (sbInfo_click) Then
                currentScreen := 10;
              If (sbExit_click) Then
                currentScreen := 11;

              // KEY EVENT
              If (IsKeyPressed(KEY_P)) Then
                If (sbFirst_active) Then
                  currentScreen := 1;
              If (IsKeyPressed(KEY_LEFT)) Then
                If (sbPrev_active) Then
                  currentScreen := 2;
              If (IsKeyPressed(KEY_RIGHT)) Then
                If (sbNext_active) Then
                  currentScreen := 3;
              If (IsKeyPressed(KEY_L)) Then
                If (sbLast_active) Then
                  currentScreen := 4;
              If (IsKeyPressed(KEY_F)) Then
                If (sbFind_active) Then
                  currentScreen := 5;
              If (IsKeyPressed(KEY_A)) Then
                If (sbAdd_active) Then
                  Begin
                    currentScreen := 6;
                    clear_add_fields;
                    clear_mod_fields;
                  End;
              If (IsKeyPressed(KEY_M)) Then
                If (sbMod_active)  Then
                  Begin
                    currentScreen := 7;
                    prepare_fields;
                  End;
              If (IsKeyPressed(KEY_D)) Then
                If (sbDel_active) Then
                  currentScreen := 8;
              If (IsKeyPressed(KEY_I)) Then
                currentScreen := 10;
              If (IsKeyPressed(KEY_X)) Then
                currentScreen := 11;

              //==================== GUI ASSISTANT =========================

// live ui design :) - assistive elements!!! - really life saver - otherwise, you have to use godot to design the UI
              // - used to find out the coordinates for speedbuttons,
              //   moving the mouse at the desired location and reading
              //   the coordinates (dragging along a 32x32 rectangle that
              //    simulated the speedbutton then used also for other elements)
              //
              //DrawText('x = ', 0,0,16,WHITE);
              //DrawText('y = ', 0,20,16,WHITE);
              //DrawText(PChar(IntToStr(round(mousePosition.x))), 56,0,16,WHITE);
              //DrawText(PChar(IntToStr(round(mousePosition.y))), 56,20,16,WHITE);
              //DrawLine(0,round(mousePosition.y),1365,round(mousePosition.y), WHITE);
              //DrawLine(round(mousePosition.x),0,round(mousePosition.x),705,WHITE);
              // resize the rectangle below as per dimensions of your widget:
              //DrawRectangle(round(mousePosition.x), round(mousePosition.y),32,32,ORANGE);
              // or uncomment the following if you need to place a label:

//DrawTextEx(fontSButton, '123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234567890', mousePosition, 14, 1, clLetter);
              // and write down the coordonates from the upper left corner of the screen.
              // I move (or just copy) this around, from a screen to another
              //   to help me in layouting the "forms".
              //================= end live ui design =======================

              // I use this initially, when no handler is defined
              //if IsKeyPressed(KEY_UP) then begin
              //  exitWindow := true; //exit
              //end;
            End;
          // =====================================================================
          // FIRST KEY "PANEL"
          // =====================================================================
          If (currentScreen = 1) Then
            Begin
              //
              clear_fields;
              d_keyfrst(NAME_FLD);
              If (db_status = S_NOTFOUND) Then
                Begin
                  squabdb_empty := true;
                End
              Else If (db_status = S_OKAY) Then
                     Begin
                       squabdb_empty := false;
                       sbFirst_active := false;
                       sbPrev_active := false;
                       If (Not sbNext_active) Then
                         Begin
                           sbLast_active := true;
                           sbNext_active := true;
                         End;
                       d_recread(@brd);
                       If (db_status = S_OKAY) Then
                         Begin
                           breed := brd.name;
                           cat := brd.category;
                           orig := brd.origin;
                           desc1 := brd.desc1;
                           desc2 := brd.desc2;
                           imgid := brd.imageid;
                         End;
                     End;
              DrawRectangle(691+48,86,372,372,clBackground);
              load_imageid(imgid, bufferTex);
              currentScreen := 0;
            End;
          // =====================================================================
          // PREV KEY "PANEL"
          // =====================================================================
          If (currentScreen = 2) Then
            Begin
              //
              clear_fields;
              d_keyprev(NAME_FLD);
              If (db_status = S_NOTFOUND) Then
                Begin
                  d_keyfrst(NAME_FLD);
                  If (db_status = S_NOTFOUND) Then
                    squabdb_empty := true
                  Else If (db_status = S_OKAY) Then
                         Begin
                           squabdb_empty := false;
                           sbFirst_active := false;
                           sbPrev_active := false;
                           If (Not sbNext_active) Then
                             Begin
                               sbNext_active := true;
                               sbLast_active := true;
                             End;
                           d_recread(@brd);
                           If (db_status = S_OKAY)Then
                             Begin
                               breed := brd.name;
                               cat := brd.category;
                               orig := brd.origin;
                               desc1 := brd.desc1;
                               desc2 := brd.desc2;
                               imgid := brd.imageid;
                             End;
                         End;
                End
              Else If (db_status = S_OKAY) Then
                     Begin
                       squabdb_empty := false;
                       sbFirst_active := true;
                       sbPrev_active := true;
                       If (Not sbNext_active) Then
                         Begin
                           sbNext_active := true;
                           sbLast_active := true;
                         End;
                       d_recread(@brd);
                       If (db_status = S_OKAY) Then
                         Begin
                           breed := brd.name;
                           cat := brd.category;
                           orig := brd.origin;
                           desc1 := brd.desc1;
                           desc2 := brd.desc2;
                           imgid := brd.imageid;
                         End;
                     End;
              DrawRectangle(691+48,86,372,372,clBackground);
              load_imageid(imgid, bufferTex);
              currentScreen := 0;
            End;
          // =====================================================================
          // NEXT KEY "PANEL"
          // =====================================================================
          If currentScreen = 3 Then
            Begin
              //
              clear_fields;
              d_keynext(NAME_FLD);
              If (db_status = S_NOTFOUND) Then
                Begin
                  d_keylast(NAME_FLD);
                  If (db_status = S_NOTFOUND) Then
                    squabdb_empty := true
                  Else If (db_status = S_OKAY) Then
                         Begin
                           squabdb_empty := false;
                           sbLast_active := false;
                           sbNext_active := false;
                           If (Not sbFirst_active) Then
                             Begin
                               sbPrev_active := true;
                               sbFirst_active := true;
                             End;
                           d_recread(@brd);
                           If (db_status = S_OKAY) Then
                             Begin
                               breed := brd.name;
                               cat := brd.category;
                               orig := brd.origin;
                               desc1 := brd.desc1;
                               desc2 := brd.desc2;
                               imgid := brd.imageid;
                             End;
                         End;
                End
              Else If (db_status = S_OKAY) Then
                     Begin
                       squabdb_empty := false;
                       sbFirst_active := true;
                       sbPrev_active := true;
                       If (Not sbFirst_active) Then
                         Begin
                           sbPrev_active := true;
                           sbFirst_active := true;
                         End;
                       d_recread(@brd);
                       If (db_status = S_OKAY) Then
                         Begin
                           breed := brd.name;
                           cat := brd.category;
                           orig := brd.origin;
                           desc1 := brd.desc1;
                           desc2 := brd.desc2;
                           imgid := brd.imageid;
                         End;
                     End;
              DrawRectangle(691+48,86,372,372,clBackground);
              load_imageid(imgid, bufferTex);
              currentScreen := 0;
            End;
          // =====================================================================
          // LAST KEY "PANEL"
          // =====================================================================
          If (currentScreen = 4) Then
            Begin
              //
              clear_fields;
              d_keylast(NAME_FLD);
              If (db_status = S_NOTFOUND) Then
                squabdb_empty := true;
              If (db_status = S_OKAY) Then
                Begin
                  squabdb_empty := false;
                  sbLast_active := false;
                  sbNext_active := false;
                  If (Not sbFirst_active) Then
                    Begin
                      sbFirst_active := true;
                      sbPrev_active := true;
                    End;
                  d_recread(@brd);
                  If (db_status = S_OKAY) Then
                    Begin
                      breed := brd.name;
                      cat := brd.category;
                      orig := brd.origin;
                      desc1 := brd.desc1;
                      desc2 := brd.desc2;
                      imgid := brd.imageid;
                    End;
                End;
              DrawRectangle(691+48,86,372,372,clBackground);
              load_imageid(imgid, bufferTex);
              currentScreen := 0;
            End;
          // =====================================================================
          // FIND KEY "PANEL"
          // =====================================================================
          If (currentScreen = 5) Then
            Begin
              cycle_common := true;
              rec_common.x := 277;
              rec_common.y := 132;
              rec_common.width := 820;
              rec_common.height := 100;
              DrawRectangleRounded(rec_common,0.4,4,DARKGRAY);
              commonPos.x := 293;
              commonPos.y := 173;
              DrawTextEx(fontLabel, 'BREED:', commonPos, 20, 1, LIGHTGRAY);
              rec_common.x := 375;
              rec_common.y := 166;
              rec_common.width := 600;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eText,60,iSearchEdit)) Then iSearchEdit :=  Not iSearchEdit
              ;
              bFindKey_click := GuiGradientButtonB(RectangleCreate (990, 166, 88, 32), 'Find', true)
              ;
              // EVENTS
              If (bFindKey_click) Then
                Begin
                  clear_buffer_fields;
                  eText := trim(eText);
                  //breed_buff := trim(eText);
                  d_keyfind(NAME_FLD, @eText);
                  If (db_status = S_OKAY) Then
                    Begin
                      d_recread(@brd);
                      If (db_status = S_OKAY) Then
                        Begin
                          breed_buff := brd.name;
                          cat_buff := brd.category;
                          orig_buff := brd.origin;
                          desc1_buff := brd.desc1;
                          desc2_buff := brd.desc2;
                          imgid_buff := brd.imageid;
                          load_imageid(imgid_buff, bufferTex2);
                        End;
                      currentScreen := 14;
                    End
                  Else currentScreen := 13;
                End;
            End;
          // =====================================================================
          // ADD RECORDS PANEL
          // =====================================================================
          If (currentScreen = 6) Then
            Begin
              //
              originated := 6;
              ClearBackground(clBackground);
              DrawRectangle(pMain_posX,pMain_posY,896,640,clPanel);
              // SECONDARY PANEL
              commonPos.x := 252;
              commonPos.y := 181;
              DrawTextEx(fontLabel, 'BREED NAME:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 215;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eBreed,60, iBreedEdit)) Then iBreedEdit := Not iBreedEdit;
              commonPos.x := 252;
              commonPos.y := 261;
              DrawTextEx(fontLabel, 'CATEGORY ("High flyer","Roller","Tumbler"):', commonPos, 20, 1,
                         clLabels);
              rec_common.x := 270;
              rec_common.y := 295;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eCategory,60,iCategoryEdit)) Then iCategoryEdit := Not
                                                                                       iCategoryEdit
              ;
              commonPos.x := 252;
              commonPos.y := 341;
              DrawTextEx(fontLabel, 'ORIGIN (Town and Country):', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 375;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eOrigin,60,iOriginEdit)) Then iOriginEdit := Not
                                                                                      iOriginEdit;
              commonPos.x := 252;
              commonPos.y := 421;
              DrawTextEx(fontLabel, 'IMAGE ID (image file name in "pictures" subfolder):', commonPos
                         , 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 455;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eImageid,60,iImageidEdit)) Then iImageidEdit := Not
                                                                                        iImageidEdit
              ;
              commonPos.x := 252;
              commonPos.y := 506;
              DrawTextEx(fontLabel, 'DESCRIPTION (100 characters on each line maxim):', commonPos,
                         20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 544;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eDesc1,100, iDesc1Edit)) Then iDesc1Edit := Not iDesc1Edit;
              rec_common.x := 270;
              rec_common.y := 580;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eDesc2,100, iDesc2Edit)) Then iDesc2Edit := Not iDesc2Edit;
              //
              commonPos.x := pMain_posX;
              commonPos.y := pMain_posY+110;
              commonPos2.x := commonPos.x + 896.0;
              commonPos2.y := commonPos.y;
              DrawLineEx(commonPos, commonPos2, 4.0, clFrames);
              DrawTexture(iAddLarge,192,65,WHITE);
              DrawTextEx(fontTitle, 'Add a new record.', Vector2Create (277,83), 56, 2, clLabels);
              bAddSave_click   := GuiGradientButtonB(RectangleCreate(461, 630, 88, 32), 'Save', true
                                  );
              bAddCancel_click := GuiGradientButtonB(RectangleCreate(791, 630, 88, 32), 'Cancel',
                                  true);
              If (bAddSave_click) Then
                Begin
                  // transfer the fields in the buffer and update the record
                  eBreed_buff := trim(eBreed);
                  eCategory_buff := trim(eCategory);
                  eOrigin_buff := trim(eOrigin);
                  eImageid_buff := trim(eImageid);
                  eDesc1_buff := trim(eDesc1);
                  eDesc2_buff := trim(eDesc2);
                  //
                  add_err := 0;
                  If (eBreed_buff = '') Then
                    Begin
                      add_err := 1;
                      err_msg := 'The key (breed) cannot be empty!';
                    End;
                  If (eCategory_buff = '') Then
                    Begin
                      add_err := 1;
                      err_msg := 'The category cannot be empty!';
                    End;
                  If (eDesc1_buff = '') Then
                    Begin
                      add_err := 1;
                      err_msg := 'The description cannot be empty!';
                    End;
                  If (eImageid_buff = '') Then
                    eImageid_buff := 'noimage.png';
                  // display error:
                  If (add_err > 0) Then currentScreen := 17;
                  If (add_err = 0) Then
                    Begin
                      //
                      // ADD NEW RECORD!
                      //
                      brd.name := eBreed_buff;
                      brd.category := eCategory_buff;
                      brd.origin := eOrigin_buff;
                      brd.desc1 := eDesc1_buff;
                      brd.desc2 := eDesc2_buff;
                      brd.imageid := eImageid_buff;
                      If (d_keyfind(NAME_FLD,@eBreed_buff) = S_NOTFOUND) Then
                        Begin
                          If (d_fillnew(BREEDS_TBL, @brd) = S_OKAY) Then
                            Begin
                              d_recread(@brd);
                              If (db_status = S_OKAY) Then
                                Begin
                                  breed := brd.name;
                                  cat := brd.category;
                                  orig := brd.origin;
                                  desc1 := brd.desc1;
                                  desc2 := brd.desc2;
                                  imgid := brd.imageid;
                                End;
                              //
                              If (squabdb_empty) Then
                                Begin
                                  squabdb_empty := false;
                                  sbFirst_active := false;
                                  sbNext_active  := true;
                                  sbFind_active  := true;
                                  sbAdd_active   := true;
                                  sbMod_active   := true;
                                  sbDel_active   := true;
                                End;
                              clear_add_fields;
                              clear_buffer_fields;
                              DrawRectangle(691+48,86,372,372,clBackground);
                              load_imageid(imgid, bufferTex);
                              currentScreen := 1;
                              // jump to the first key in the database
                            End
                          Else writeln('Error adding a new record!');
                        End
                      Else
                        Begin
                          currentScreen := 17;
                          err_msg := 'Sorry, this breed is already here!';
                        End;
                    End;
                End;
              If (bAddCancel_click) Then
                Begin
                  // no transfer...
                  currentScreen := 0;
                End;
            End;
          // =====================================================================
          // MODIFY RECORDS PANEL
          // =====================================================================
          If (currentScreen = 7) Then
            Begin
              //
              originated := 7;
              ClearBackground(clBackground);
              DrawRectangle(pMain_posX,pMain_posY,896,640,clPanel);
              // SECONDARY PANEL
              commonPos.x := 252;
              commonPos.y := 181;
              DrawTextEx(fontLabel, 'BREED NAME:', commonPos, 20, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 215;
              DrawTextEx(fontLetter, @breed[1], commonPos, 24,1,clLabels);
              commonPos.x := 252;
              commonPos.y := 261;
              DrawTextEx(fontLabel, 'CATEGORY:', commonPos, 20, 1, clLabels);
              If (GuiTextBox(RectangleCreate(270,295,760,32),@eCategory[0],60,iCategoryEdit)) Then
                iCategoryEdit := Not iCategoryEdit;
              commonPos.x := 252;
              commonPos.y := 341;
              DrawTextEx(fontLabel, 'ORIGIN:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 375;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eOrigin,60,iOriginEdit)) Then iOriginEdit := Not
                                                                                      iOriginEdit;
              commonPos.x := 252;
              commonPos.y := 421;
              DrawTextEx(fontLabel, 'IMAGE ID:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 455;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eImageid,60,iImageidEdit)) Then iImageidEdit := Not
                                                                                        iImageidEdit
              ;
              commonPos.x := 252;
              commonPos.y := 506;
              DrawTextEx(fontLabel, 'DESCRIPTION:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 544;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eDesc1,100, iDesc1Edit)) Then iDesc1Edit := Not iDesc1Edit;
              rec_common.x := 270;
              rec_common.y := 580;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,@eDesc2,100, iDesc2Edit)) Then iDesc2Edit := Not iDesc2Edit;
              //
              commonPos.x := pMain_posX;
              commonPos.y := pMain_posY+110;
              commonPos2.x := commonPos.x + 896.0;
              commonPos2.y := commonPos.y;
              DrawLineEx(commonPos, commonPos2, 4.0, clFrames);
              DrawTexture(iModLarge,201,65,WHITE);
              DrawTextEx(fontTitle, 'Modify the record (navigation).', Vector2Create(277,83), 56, 2,
              clLabels);
              bModSave_click := GuiGradientButtonB(RectangleCreate(461, 630, 88, 32), 'Save', true);
              bModCancel_click := GuiGradientButtonB(RectangleCreate(791, 630, 88, 32), 'Cancel',
                                  true);
              If (bModSave_click) Then
                Begin
                  mod_err := 0;
                  // transfer the fields in the buffer and update the record if no err
                  cat := trim(eCategory);
                  orig := trim(eOrigin);
                  imgid := trim(eImageid);
                  desc1 := trim(eDesc1);
                  desc2 := trim(eDesc2);
                  If (cat = '') Then
                    Begin
                      mod_err := 1;
                      err_msg := 'Category cannot be empty!';
                    End;
                  If (orig = '') Then
                    Begin
                      mod_err := 1;
                      err_msg := 'Origin cannot be empty!';
                    End;
                  If (desc1 = '') Then
                    Begin
                      mod_err := 1;
                      err_msg := 'Description cannot be empty!';
                    End;
                  If (imgid = '') Then
                    Begin
                      imgid := 'noimage.png';
                    End;
                  If (mod_err > 0) Then currentScreen := 17;
                  If (mod_err = 0) Then
                    Begin
                      currentScreen := 0;
                      //
                      // UPDATE THE RECORD!
                      //
                      brd.name := breed;
                      brd.category := cat;
                      brd.origin := orig;
                      brd.desc1 := desc1;
                      brd.desc2 := desc2;
                      brd.imageid := imgid;
                      If (d_recwrite(@brd) = S_OKAY ) Then
                        Begin
                          writeln('Record updated!');
                          d_recread(@brd);
                          If (db_status = S_OKAY) Then
                            Begin
                              breed := brd.name;
                              cat := brd.category;
                              orig := brd.origin;
                              desc1 := brd.desc1;
                              desc2 := brd.desc2;
                              imgid := brd.imageid;
                            End;
                        End
                      Else
                        writeln('Error updating the record!');
                    End;
                End;
              If (bModCancel_click) Then
                Begin
                  // no transfer...
                  currentScreen := 0;
                  d_recread(@brd);
                  If (db_status = S_OKAY) Then
                    Begin
                      breed := brd.name;
                      cat := brd.category;
                      orig := brd.origin;
                      desc1 := brd.desc1;
                      desc2 := brd.desc2;
                      imgid := brd.imageid;
                    End;
                End;
            End;
          // =====================================================================
          // DELETE RECORDS "PANEL"
          // =====================================================================
          If currentScreen = 8 Then
            Begin
              //
              rec_common.x := 326;
              rec_common.y := 508;
              rec_common.width := 660;
              rec_common.height := 100;
              DrawRectangleRounded(rec_common, 0.2,4,clBrick);
              rec_common.x := 441;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              rec_common.x := 771;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              commonPos.x := 501;
              commonPos.y := 542;
              DrawTextEx(fontLabel, 'Do you really want to delete this record?', commonPos, 20, 1,
                         WHITE);
              bDelYes_click := GuiGradientButtonB(RectangleCreate(461, 590, 60, 28), 'Yes', true);
              bDelNo_click := GuiGradientButtonB(RectangleCreate(791, 590, 60, 28), 'No', true);
              If bDelNo_click Then
                currentScreen := 0;
              If bDelYes_click Then
                Begin
                  currentScreen := 1;
                  // go to reading the first key...
                  If (d_delete() = S_OKAY) Then writeln('Record deleted!')
                  Else  writeln('Error deleting the record!');
                End;
            End;
          // =====================================================================
          // ABOUT PANEL
          // =====================================================================
          If currentScreen = 10 Then
            Begin
              stp := 60;
              ClearBackground(clBackground);
              DrawTexture(iGrass1a, pSplash_posX-82, pSplash_posY + 285, WHITE);
              DrawTexture(iAbout, 890, 11, WHITE);
              DrawRectangle(pSplash_posX,pSplash_posY,896-200,664-204,clPanel);
              //DrawTexture(iPigeon_Bye, 200,100, WHITE);
              If cbTheme_idx = 0 Then
                Begin
                  // dark theme
                  DrawTexture(iFirefly2, 236, 494, WHITE);
                  DrawTexture(iFirefly1, 298, 478, WHITE);
                End
              Else // light theme
                DrawTexture(iLadybug, 291, 516, WHITE);
              //
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + 10);
              DrawTextEx(fontTitle, 'Actually, I wanna give thanks:', commonPos, 56, 2, clLabels);
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + stp + 75);
              DrawTextEx(fontLetter, '- to Lorena, my daughter for graphics advising;', commonPos,
                         24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + stp + 103);
              DrawTextEx(fontLetter, '- to FreePascal team and forums;', commonPos, 24, 2, clLabels)
              ;
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + stp + 131);
              DrawTextEx(fontLetter, '- to Gunko Vadim for raylib pascal translation;', commonPos,
                         24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + stp + 159);
              DrawTextEx(fontLetter, '- to Thomas B. Pedersen, original developer of typhoon;',
                         commonPos, 24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + stp + 187);
              DrawTextEx(fontLetter, '- to Kaz Kylheku for update on typhoon library;', commonPos,
                         24, 2, clLabels);
              commonPos.x := single(pSplash_posX + 20);
              commonPos.y := single(pSplash_posY + stp + 215);
              DrawTextEx(fontLetter, '- to Ramon Santamaria and raylib development team.', commonPos
                         , 24, 2, clLabels);
              bQuitYes_click := GuiGradientButtonB(RectangleCreate(pSplash_posX + 315, pSplash_posY
                                + 398, 88, 32), 'OK', true);
              If bQuitYes_click Then
                Begin
                  currentScreen := 0;
                End;
              //
            End;
          // =====================================================================
          // EXIT PANEL
          // =====================================================================
          If currentScreen = 11 Then
            Begin
              ClearBackground(clBackground);
              DrawRectangle(pSplash_posX,pSplash_posY,896-200,664-204,clPanel);
              DrawTexture(iPigeon_Bye, 200,100, WHITE);
              commonPos.x := single(pSplash_posX + 320);
              commonPos.y := single(pSplash_posY + 130);
              DrawTextEx(fontTitle, 'Quitting?', commonPos, 56, 2, clLabels);
              commonPos.x := single(pSplash_posX + 280);
              commonPos.y := single(pSplash_posY + 230);
              DrawTextEx(fontLetter, '... it seems you wanna leave...', commonPos, 24, 2, clLabels);
              DrawTexture(iSlime1, pSplash_posX + 105,pSplash_posY + 420, WHITE);
              DrawTexture(iSlime2, pSplash_posX + 136,pSplash_posY + 355, WHITE);
              bQuitYes_click := GuiGradientButtonB(RectangleCreate(pSplash_posX + 100, pSplash_posY
                                + 398, 88, 32), 'Yes', true);
              bQuitNo_click := GuiGradientButtonB(RectangleCreate(pSplash_posX + 530, pSplash_posY +
                               398, 88, 32), 'No', true);
              If bQuitYes_click Then
                Begin
                  exitWindow := true;
                End;
              If bQuitNo_click Then
                Begin
                  currentScreen := 0;
                End;
            End;
          // =====================================================================
          // ERROR NEXTKEY "PANEL" - not used, reminiscence from qdbm database
          // =====================================================================
          If currentScreen = 12 Then
            Begin
              //ClearBackground(clBackground);
              rec_common.x := 326;
              rec_common.y := 508;
              rec_common.width := 660;
              rec_common.height := 100;
              DrawRectangleRounded(rec_common, 0.2,4,clBrick);
              rec_common.x := 611;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              commonPos.x := 546;
              commonPos.y := 542;
              DrawTextEx(fontLabel, 'Already at the last record!', commonPos, 20, 1, WHITE);
              bErrorNextKey_click := GuiGradientButtonB(RectangleCreate(631, 590, 60, 28), 'Ok',
                                     true);
              If bErrorNextKey_click Then
                Begin
                  currentScreen := originated;
                  bErrorNextKey_click := false;
                End;
            End;
          // =====================================================================
          // ERROR FIND KEY "PANEL"
          // =====================================================================
          If currentScreen = 13 Then
            Begin
              //ClearBackground(clBackground);
              rec_common.x := 326;
              rec_common.y := 508;
              rec_common.width := 660;
              rec_common.height := 100;
              DrawRectangleRounded(rec_common, 0.2,4,clBrick);
              rec_common.x := 611;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              commonPos.x := 540;
              commonPos.y := 542;
              DrawTextEx(fontLabel, 'Sorry, there is no such key!', commonPos, 20, 1, WHITE);
              bErrorFindKey_click := GuiGradientButtonB(RectangleCreate(631, 590, 60, 28), 'Ok',
                                     true);
              If (bErrorFindKey_click Or IsKeyPressed(KEY_ENTER)) Then
                Begin
                  currentScreen := 3;
                  // we go to next similar key
                  bErrorFindKey_click := false;
                  sbLast_active := true;
                  sbNext_active := true;
                  sbFirst_active := true;
                  sbPrev_active := true;
                End;
            End;
          // =====================================================================
          // FIND RESULT PANEL
          // =====================================================================
          If (currentScreen = 14) Then
            Begin
              //
              originated := 14;
              ClearBackground(clBackground);
              DrawRectangle(pMain_posX,pMain_posY,896,600,clPanel);
              commonPos.x := 252;
              commonPos.y := 181;
              DrawTextEx(fontLabel, 'BREED NAME:', commonPos, 20, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 215;
              If (breed_buff = '') Then
                DrawTextEx(fontLetter, '...', commonPos, 24, 1, clLabels)
              Else
                DrawTextEx(fontLetter, @breed_buff[1], commonPos, 24, 1, clLabels);
              commonPos.x := 252;
              commonPos.y := 261;
              DrawTextEx(fontLabel, 'CATEGORY:', commonPos, 20, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 295;
              If (cat_buff = '') Then
                DrawTextEx(fontLetter, '...', commonPos, 24, 1, clLabels)
              Else
                DrawTextEx(fontLetter, @cat_buff[1], commonPos, 24, 1, clLabels);
              commonPos.x := 252;
              commonPos.y := 341;
              DrawTextEx(fontLabel, 'ORIGIN:', commonPos, 20, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 375;
              If (orig_buff = '') Then
                DrawTextEx(fontLetter, '...', commonPos, 24, 1, clLabels)
              Else
                DrawTextEx(fontLetter, @orig_buff[1], commonPos, 24, 1, clLabels);
              commonPos.x := 252;
              commonPos.y := 506;
              DrawTextEx(fontLabel, 'DESCRIPTION:', commonPos, 20, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 544;
              DrawTextEx(fontDesc, @desc1_buff[1], commonPos, 16, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 584;
              DrawTextEx(fontDesc, @desc2_buff[1], commonPos, 16, 1, clLabels);
              //
              // draw lines and photo frame
              commonPos.x := pMain_posX;
              commonPos.y := pMain_posY+110;
              commonPos2.x := commonPos.x + 896.0;
              commonPos2.y := commonPos.y;
              DrawLineEx(commonPos, commonPos2, 4.0, clFrames);
              DrawTexture(iFindLarge,189,13,WHITE);
              DrawTextEx(fontTitle, 'I Found It!', Vector2Create(337,83), 56, 2, clLabels);
              rec_common.x := 677;
              rec_common.y := 72;
              rec_common.width := 400;
              rec_common.height := 400;
              DrawRectangleRounded(rec_common,0.1,4,clPhoto);
              If (imgid_buff = '') Then
                DrawTexture(iNoImage,691,86,WHITE)
              Else
                DrawTexture(bufferTex2,691,86,WHITE);

              bResultMod_click := GuiGradientButtonB(RectangleCreate(762-28, 480, 88, 32), 'Modify',
                                  true);
              bResultDel_click := GuiGradientButtonB(RectangleCreate(862-28, 480, 88, 32), 'Delete',
                                  true);
              bResultClose_click := GuiGradientButtonB(RectangleCreate(962-28, 480, 88, 32), 'Close'
                                    , true);

              If ((bResultClose_click) Or (IsKeyPressed(KEY_C))) Then
                Begin
                  currentScreen := 1;
                  bResultClose_click := false;
                End;
              If ((bResultDel_click) Or (IsKeyPressed(KEY_D))) Then
                Begin
                  currentScreen := 15;
                  bResultDel_click := false;
                End;
              If ((bResultMod_click) Or (IsKeyPressed(KEY_M))) Then
                Begin
                  currentScreen := 16;
                  bResultMod_click := false;
                  eCategory_buff := cat_buff;
                  eOrigin_buff := orig_buff;
                  eImageid_buff := imgid_buff;
                  eDesc1_buff := desc1_buff;
                  eDesc2_buff := desc2_buff;
                End;
            End;
          // =====================================================================
          // FIND RESULT DELETE - PANEL
          // =====================================================================
          If currentScreen = 15 Then
            Begin
              originated := 14;
              rec_common.x := 326;
              rec_common.y := 508;
              rec_common.width := 660;
              rec_common.height := 100;
              DrawRectangleRounded(rec_common, 0.2,4,clBrick);
              rec_common.x := 441;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              rec_common.x := 771;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              commonPos.x := 501;
              commonPos.y := 542;
              DrawTextEx(fontLabel, 'Do you really want to delete this record?', commonPos, 20, 1,
                         WHITE);
              bResultDelYes_click := GuiGradientButtonB(RectangleCreate(461, 590, 60, 28), 'Yes',
                                     true);
              bResultDelNo_click := GuiGradientButtonB(RectangleCreate(791, 590, 60, 28), 'No', true
                                    );
              If (bResultDelNo_click Or IsKeyPressed(KEY_N)) Then
                currentScreen := 14;
              If (bResultDelYes_click  Or IsKeyPressed(KEY_Y)) Then
                Begin
                  currentScreen := 1;
                  If (d_delete() <> S_OKAY) Then
                    writeln('Record cannot be deleted!');
                End;
            End;
          // =====================================================================
          // MODIFY RECORDS PANEL
          // =====================================================================
          If (currentScreen = 16) Then
            Begin
              originated := 16;
              ClearBackground(clBackground);
              DrawRectangle(pMain_posX,pMain_posY,896,640,clPanel);
              // SECONDARY PANEL
              commonPos.x := 252;
              commonPos.y := 181;
              DrawTextEx(fontLabel, 'BREED NAME:', commonPos, 20, 1, clLabels);
              commonPos.x := 270;
              commonPos.y := 215;
              DrawTextEx(fontLetter, @breed_buff[1],commonPos, 24,1,clLabels);
              commonPos.x := 252;
              commonPos.y := 261;
              DrawTextEx(fontLabel, 'CATEGORY:', commonPos, 20, 1, clLabels);
              If (GuiTextBox(RectangleCreate(270,295,760,32),eCategory_buff,60,iCategoryEdit)) Then
                iCategoryEdit := Not iCategoryEdit;
              commonPos.x := 252;
              commonPos.y := 341;
              DrawTextEx(fontLabel, 'ORIGIN:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 375;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,eOrigin_buff,60,iOriginEdit)) Then iOriginEdit :=  Not
                                                                                         iOriginEdit
              ;
              commonPos.x := 252;
              commonPos.y := 421;
              DrawTextEx(fontLabel, 'IMAGE ID:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 455;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,eImageid_buff,60,iImageidEdit)) Then iImageidEdit := Not
                                                                                        iImageidEdit
              ;
              commonPos.x := 252;
              commonPos.y := 506;
              DrawTextEx(fontLabel, 'DESCRIPTION:', commonPos, 20, 1, clLabels);
              rec_common.x := 270;
              rec_common.y := 544;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,eDesc1_buff,100, iDesc1Edit)) Then iDesc1Edit := Not
                                                                                         iDesc1Edit;
              rec_common.x := 270;
              rec_common.y := 580;
              rec_common.width := 760;
              rec_common.height := 32;
              If (GuiTextBox(rec_common,eDesc2_buff,100, iDesc2Edit)) Then iDesc2Edit := Not
                                                                                         iDesc2Edit;
              //
              commonPos.x := pMain_posX;
              commonPos.y := pMain_posY+110;
              commonPos2.x := commonPos.x + 896.0;
              commonPos2.y := commonPos.y;
              DrawLineEx(commonPos, commonPos2, 4.0, clFrames);
              DrawTexture(iModLarge,201,65,WHITE);
              DrawTextEx(fontTitle, 'Modify the record (search result).', Vector2Create(277,83), 56,
              2, clLabels);
              bModSave_click := GuiGradientButtonB(RectangleCreate (461, 630, 88, 32), 'Save', true)
              ;
              bModCancel_click := GuiGradientButtonB(RectangleCreate(791, 630, 88, 32), 'Cancel',
                                  true);
              If (bModSave_click) Then
                Begin
                  mod_err := 0;
                  // transfer the fields in the buffer and update the record if no err
                  cat_buff := trim(eCategory_buff);
                  orig_buff := trim(eOrigin_buff);
                  imgid_buff := trim(eImageid_buff);
                  desc1_buff := trim(eDesc1_buff);
                  desc2_buff := trim(eDesc2_buff);
                  If (cat_buff = '') Then
                    Begin
                      mod_err := 1;
                      err_msg := 'Category cannot be empty!';
                    End;
                  If (orig_buff = '') Then
                    Begin
                      mod_err := 1;
                      err_msg := 'Origin cannot be empty!';
                    End;
                  If (desc1_buff = '') Then
                    Begin
                      mod_err := 1;
                      err_msg := 'Description cannot be empty!';
                    End;
                  If (imgid_buff = '') Then
                    imgid_buff := 'noimage.png';
                  If (mod_err > 0) Then currentScreen := 17;
                  If (mod_err = 0) Then
                    Begin
                      currentScreen := 14;
                      brd.name := breed_buff;
                      brd.category := cat_buff;
                      brd.origin := orig_buff;
                      brd.desc1 := desc1_buff;
                      brd.desc2 := desc2_buff;
                      brd.imageid := imgid_buff;
                      If (d_recwrite(@brd) = S_OKAY ) Then
                        Begin
                          writeln('Record updated!');
                        End;
                      load_imageid(imgid_buff, bufferTex2);
                    End;
                End;
              If (bModCancel_click) Then
                Begin
                  // no transfer...
                  currentScreen := 14;
                End;
            End;
          // =====================================================================
          // ERRORS FROM ADD AND MOD SCREENS
          // =====================================================================
          If (currentScreen = 17) Then
            Begin
              //
              rec_common.x := 326;
              rec_common.y := 508;
              rec_common.width := 660;
              rec_common.height := 100;
              DrawRectangleRounded(rec_common, 0.2,4,clBrick);
              rec_common.x := 611;
              rec_common.y := 589;
              rec_common.width := 100;
              rec_common.height := 40;
              DrawRectangleRounded(rec_common, 0.6,4,clBrick);
              commonPos.x := 440;
              commonPos.y := 542;
              DrawTextEx(fontLabel, @err_msg[1], commonPos, 20, 1, WHITE);
              bErrorAddKey_click := GuiGradientButtonB(RectangleCreate(631, 590, 60, 28), 'Ok', true
                                    );
              If ((bErrorAddKey_click) Or (IsKeyPressed(KEY_ENTER))) Then
                Begin
                  currentScreen := originated;
                  bErrorAddKey_click := false;
                End;
            End;
          //////////////////////
          EndDrawing();
        End;
      d_close();
      // close database
      // De-Initialization
      UnloadTexture(sbDel_gray);
      UnloadTexture(sbMod_gray);
      UnloadTexture(sbAdd_gray);
      UnloadTexture(sbFind_gray);
      UnloadTexture(sbLast_gray);
      UnloadTexture(sbNext_gray);
      UnloadTexture(sbPrev_gray);
      UnloadTexture(sbFirst_gray);
      //
      UnloadTexture(sbExit);
      UnloadTexture(sbInfo);
      UnloadTexture(sbDel);
      UnloadTexture(sbMod);
      UnloadTexture(sbAdd);
      UnloadTexture(sbFind);
      UnloadTexture(sbLast);
      UnloadTexture(sbNext);
      UnloadTexture(sbPrev);
      UnloadTexture(sbFirst);
      //
      UnloadTexture(iGrass2b);
      UnloadTexture(iGrass1a);
      UnloadTexture(iSlime2);
      UnloadTexture(iSlime1);
      UnloadTexture(iAbout);
      UnloadTexture(iPigeon_Bye);
      UnloadTexture(iPigeon_Welcome);
      //
      UnloadFont(fontDesc);
      UnloadFont(fontSButton);
      UnloadFont(fontButtonBold);
      UnloadFont(fontButton);
      UnloadFont(fontLabel);
      UnloadFont(fontLetter);
      UnloadFont(fontTitle);
      //
      UnloadTexture(iNoImage);
      UnloadTexture(bufferTex);
      UnloadTexture(bufferTex2);
      UnloadTexture(iFirefly1);
      UnloadTexture(iFirefly2);
      UnloadTexture(iLadybug);
      UnloadTexture(iFindLarge);
      UnloadTexture(iModLarge);
      UnloadTexture(iAddLarge);
      //--------------------------------------------------------------------
      CloseWindow();
      // Close window and OpenGL context
      //--------------------------------------------------------------------
    End
  Else writeln('Error opening database!');
End.
