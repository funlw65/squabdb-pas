# your program executable name (no spaces)
PROGRAM=squabdb
# PATH for libraries, includes and units
LIBPATH=-Fl$(HOME)/fpc/libs
INCPATH=-Fi$(HOME)/fpc/inc
UNITSPATH=-Fu$(HOME)/fpc/units/x86_64-linux/*
# compilation flags
FPCFLAGS=-TLinux -Mfpc -Sgisc -CgX -Os -CpATHLON64 -OpCOREI -XS -vewnhibq

all:
	~/fpc/fpc-ootb-64 $(LIBPATH) $(INCPATH) $(UNITSPATH) $(FPCFLAGS)  $(PWD)/$(PROGRAM).pas

# You issue this target only once IF the datatydb.dbd file does not exist!
# Then you edit the datatydb.pas to eliminate the duplicates.
# Intially, this example comes with the database created and populated,
# no need to create it from this Makefile.
#createdb:
#	~/fpc/tools/ddlp  datatydb.ddl
#	~/fpc/tools/h2pas  datatydb.h -o datatydb.pas

# NOTE: unit $(PROGRAM)ty.pas myst be edited as per README file...

clean:
	rm *.o *.ppu $(PROGRAM)
