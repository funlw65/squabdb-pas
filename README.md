# SquabDB
SquabDB is a little Pigeon Database intended as a typhoon database engine demonstration. Written in Pascal language.
The graphical interface uses raylib 4.2 and raygui 3.2

![The main form](doc/main.png)

## Requirements
- fpc compiler that must be downloaded from the link below as it contains all libs and tools required.     
- see it [here](https://gitlab.com/funlw65-mine/fpc)

Just clone the fpc repository it in your $HOME folder. If there is already a fpc folder, move it or rename it.

# Compilation
## Creating the database and the pascal unit that contains the details

NOTE: The database is already created and populated, avoid this step to see the app in action.

1. Issue the command "make createdb" if it does not exist, then you have to edit the squabdbty.pas unit that contains the definitions of the databases and fields - as freepascal is not case sensitive, there will be compulations errors without your intervention. By example: To be continued... 


