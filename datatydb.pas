
unit datatydb;
interface

{
  Automatically converted by H2Pas 1.0.0 from datatydb.h
  The following command line parameters were used:
    datatydb.h
    -o
    datatydb.pas
}

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}


  {---------- headerfile for datatydb.ddl ---------- }
  { alignment is 8  }
  {---------- structures ---------- }
  { size 391  }

  type
    breeds = record
        name : array[0..59] of char;
        category : array[0..10] of char;
        origin : array[0..59] of char;
        imageid : array[0..59] of char;
        desc1 : array[0..99] of char;
        desc2 : array[0..99] of char;
      end;

  {---------- record names ---------- }

  const
    BREEDS_tbl = 1000;    
  {---------- field names ---------- }
    NAME_fld = 1001;    
    CATEGORY_fld = 1002;    
    ORIGIN_fld = 1003;    
    IMAGEID_fld = 1004;    
    DESC1_fld = 1005;    
    DESC2_fld = 1006;    
  {---------- key names ---------- }
  {---------- sequence names ---------- }
  {---------- integer constants ---------- }

implementation


end.
