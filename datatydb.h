/*---------- headerfile for datatydb.ddl ----------*/
/* alignment is 8 */

/*---------- structures ----------*/
struct breeds {  /* size 391 */
    char    name[60];
    char    category[11];
    char    origin[60];
    char    imageid[60];
    char    desc1[100];
    char    desc2[100];
};

/*---------- record names ----------*/
#define BREEDS 1000L

/*---------- field names ----------*/
#define NAME 1001L
#define CATEGORY 1002L
#define ORIGIN 1003L
#define IMAGEID 1004L
#define DESC1 1005L
#define DESC2 1006L

/*---------- key names ----------*/

/*---------- sequence names ----------*/

/*---------- integer constants ----------*/
